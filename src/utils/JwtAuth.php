<?php


namespace sim\utils;


use Firebase\JWT\JWT;

class JwtAuth
{
    protected $token;

    public function createToken(string $host, string $key, array $data = [])
    {
        $time = time();
        $payload = [
            'iss' => $host, // jwt签发者
            'aud' => $host, // 接收jwt的一方
            'iat' => $time, //签发时间
            'nbf' => $time , //(Not Before)：某个时间点后才能访问，比如设置time+30，表示当前时间30秒后才能使用
            'exp' => $time + 7200, //过期时间,这里设置2个小时
            'data' => $data
        ];
        return JWT::encode($payload, $key);
    }

    public function parseToken(string $token)
    {
        $this->token = $token;
        [$head, $body, $sign] = explode('.', $this->token);
        $body = JWT::jsonDecode(JWT::urlsafeB64Decode($body));
        return $body;
    }

    public function verifyToken(string $key)
    {
        JWT::$leeway = 60;//当前时间减去60，把时间留点余地
        JWT::decode($this->token, $key, ['HS256']); //HS256方式，这里要和签发的时候对应
    }
}