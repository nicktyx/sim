<?php


namespace sim\utils;

/**
 * 字符类型操作类
 * Class Json
 * @package crmeb\utils
 */
class Str
{
    /**
     * 获取随机字符串
     * @param int $length
     * @return string
     */
    public static function getRanStr(int $length = 4)
    {
        $chars = [
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
            "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
            "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
            "3", "4", "5", "6", "7", "8", "9"
        ];
        $charsLen = count($chars) - 1;
        shuffle($chars);
        $output = "";
        for ($i = 0; $i < $length; $i++)
        {
            $output .= $chars[mt_rand(0, $charsLen)];
        }
        return $output;
    }

    /**
     * 生成密码
     * @param $password
     * @param $salt
     * @return string
     */
    public static function getMd5Password(string $password, string $salt)
    {
        return md5(md5($password) . $salt);
    }
}